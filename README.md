# GraphQL Injection Story

**To read**: <https://cheatsheetseries.owasp.org/cheatsheets/GraphQL_Cheat_Sheet.html>

**Estimated reading time**: 15 min

## Story Outline

This story is focused on providing guidance for preventing GraphQL Injection flaws
in your applications.

## Story Organization

**Story Branch**: main
> `git checkout main`

**Practical task tag for self-study**: task
>`git checkout task`

Tags: #cpp, #aspnetcore, #graphql, #security
