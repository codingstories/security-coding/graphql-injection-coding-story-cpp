#include "AuthenticationApp.h"

int main() {
    grp_inj::AuthenticationApp app;
    app.run();
    
    return 0;
}
