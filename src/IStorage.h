#pragma once
#include <string>
#include <memory>

namespace grp_inj {

    class IStorage {
        public:
            virtual ~IStorage() = default;
            /*
             * \brief execute query
             */  
            virtual bool query(std::string_view query) const = 0;
    };

    using StoragePtr = std::unique_ptr<IStorage>;
    StoragePtr createStorage();

} // namespace grp_inj
